#include<iostream>
#include<algorithm>
#include<vector>
#include<set>
#include<map>

using namespace std;

int pracownicy[100001];
vector<int>koledzy[100001];
vector<int>wyniki;
int liczba_pracownikow;

int liczba_kolegow(int pracownik, int flaga) {
	int liczba = 0;
	for(int i = 0; i < koledzy[pracownik].size(); ++i) {
		if(pracownicy[koledzy[pracownik][i]] == flaga)++liczba;
	}
	return liczba;
}

void oblicz(int pracownik, int flaga) {
	int roznica = 0;
	int nastepny = 0;
	vector<int>grupa;
	map<int, int>flag;
	for(int i = 0; i < koledzy[pracownik].size(); ++i) {
		if(pracownicy[koledzy[pracownik][i]] == flaga) {
			++pracownicy[koledzy[pracownik][i]];
			grupa.push_back(koledzy[pracownik][i]);
			flag[koledzy[pracownik][i]] = 1;
			++roznica;
		}
	}
	int aktualny_stan = 1;
	int tmp_roznica = roznica;
	set<int>to_check;
	bool rep = true;
	int pop = 0, licz = 0;
	while(rep) {
		rep = false;
		for(int i = 0; i < grupa.size(); ++i) {
			if(flag[grupa[i]] == aktualny_stan && liczba_kolegow(grupa[i], flaga) != liczba_pracownikow - tmp_roznica) {
				//pracownicy[grupa[i]] = 0;
				flag[grupa[i]] = 0;
				//usuniete.insert(grupa[i]);
				licz = 0;
				for(int ii = 0; ii < koledzy[grupa[i]].size(); ++ii) {
					int p = koledzy[grupa[i]][ii];
					if(flag.find(p) != flag.end() && flag[p] == aktualny_stan) {
						flag[p] = aktualny_stan + 1;
						++licz;
					}
				}
				if(licz != pop)rep = true;
				pop = licz;
				++aktualny_stan;
			}
			else if(flag[grupa[i]] != aktualny_stan) {
				licz = 0;
				for(int ii = 0; ii < koledzy[grupa[i]].size(); ++ii) {
					int p = koledzy[grupa[i]][ii];
					if(flag.find(p) != flag.end() && flag[p] == aktualny_stan) {
						flag[p] = aktualny_stan + 1;
						++licz;
					}
				}
				if(licz != pop)rep = true;
				pop = licz;
				++aktualny_stan;
			}

		}

	}
	for(map<int, int>::iterator it = flag.begin(); it != flag.end(); ++it) {
		if(it->second == aktualny_stan) nastepny = it->first;
		else {
			pracownicy[it->first] = 0;
			--roznica;
		}
	}
	wyniki.push_back(liczba_pracownikow-roznica);
	liczba_pracownikow = roznica;
	if(liczba_pracownikow) {
		return oblicz(nastepny, flaga+1);
	}
}



int main() {
	ios_base::sync_with_stdio(0);
	int liczba_polaczen;
	cin>>liczba_pracownikow>>liczba_polaczen;
	int x, y;
	for(int i = 0; i < liczba_polaczen; ++i) {
		cin>>x>>y;
		koledzy[x].push_back(y);
		koledzy[y].push_back(x);
	}
	oblicz(1, 0);
	cout<<wyniki.size()<<endl;
	sort(wyniki.begin(), wyniki.end());
	for(int i = 0; i < wyniki.size(); ++i) {
		cout<<wyniki[i]<<" ";
	}
	return 0;
}
